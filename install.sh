 #!/bin/bash

set -ex

cd `dirname $0`

#
tar xvf ./ntp-4.2.8p10.tar.gz
./ntp-4.2.8p10/configure

#
mv ./libinno ./ntp-4.2.8p10

#
patch -R ./ntp-4.2.8p10/ntpd/ntp_proto.c < ./ntpd/ntp_proto.patch
patch -R ./ntp-4.2.8p10/ntpd/Makefile < ./ntpd/Makefile.patch
