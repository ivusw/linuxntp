///////////////////////////////////////////////////////////////////////////////
//  _____                                  _
// |_   _|                                (_)
//   | |  _ __  _ __   _____   ___   _ ___ _  ___  _ __
//   | | | '_ \| '_ \ / _ \ \ / / | | / __| |/ _ \| '_  |
//  _| |_| | | | | | | (_) \ V /| |_| \__ \ | (_) | | | |
// |_____|_| |_|_| |_|\___/ \_/  \__,_|___/_|\___/|_| |_|
//
//
// Copyright (c) 2018  Innovusion, Inc.
// All Rights Reserved.
//
//    iv_packet.h
//
///////////////////////////////////////////////////////////////////////////////

#ifndef SRC_IV_PACKET_H_
#define SRC_IV_PACKET_H_

#include <stdint.h>

extern "C" {
    typedef void(*LogFunc)(const char* str);
    void on_start(LogFunc log);

    void on_ntp_packet(int64_t offset_ns, uint32_t flags);

    //
    void print_config();
    void print_time_reg();
}

#endif /* SRC_IV_PACKET_H_ */
