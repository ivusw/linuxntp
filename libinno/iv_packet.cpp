///////////////////////////////////////////////////////////////////////////////
//  _____                                  _
// |_   _|                                (_)
//   | |  _ __  _ __   _____   ___   _ ___ _  ___  _ __
//   | | | '_ \| '_ \ / _ \ \ / / | | / __| |/ _ \| '_  |
//  _| |_| | | | | | | (_) \ V /| |_| \__ \ | (_) | | | |
// |_____|_| |_|_| |_|\___/ \_/  \__,_|___/_|\___/|_| |_|
//
//
// Copyright (c) 2018  Innovusion, Inc.
// All Rights Reserved.
//
//    iv_packet.c
//
///////////////////////////////////////////////////////////////////////////////

#include "iv_packet.h"
#include "config.h"

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <cmath>

/* Basic bit manipulation macros */
// Set bit y (0-indexed) of x to '1' by generating a mask with a '1' in the
// proper bit location and ORing x with the mask.
#define SET_BIT(x, y) (x) |= (1 << (y))

// Set bit y (0-indexed) of x to '0' by generating a mask with a '0' in the
// y position and 1's elsewhere then ANDing the mask with x.
#define CLEAR_BIT(x, y) (x) &= ~(1 << (y))

// Return '1' if the bit value at position y within x is '1' and '0' if it's 0
// by ANDing x with a bit mask where the bit in y's position is '1' and '0'
// elsewhere and comparing it to all 0's.  Returns '1' in least significant bit
// position if the value of the bit is '1', '0' if it was '0'.
#define READ_BIT(x, y) ((0u == ((x) & (1 << (y))) ? 0u : 1u))

// Toggle bit y (0-index) of x to the inverse: '0' becomes '1', '1' becomes '0'
// by XORing x with a bitmask where the bit in position y is '1' and all others
// are '0'.
#define TOGGLE_BIT(x, y) ((x) ^= (1 << (y)))

// Update a subset of an integer defined by the starting index to update and the
// bitwidth of the target region. First create a mask using the bitwidth, then
// clear to update by shifting the mask by the requested index, finally update
// the region using the y value (bounded by the mask)
#define UPDATE_BITS(x, y, idx, bitwidth)     \
  do {                                       \
    uint32_t mask = ((1 << (bitwidth)) - 1); \
    (x) &= ~((mask) << (idx));               \
    (x) |= (((y) & (mask)) << (idx));        \
  } while (0)

///////////////////////////////////////////////////////////////////////////////
// config

//
// config
//
NtpConfig NTP_CONFIG;


///////////////////////////////////////////////////////////////////////////////
// log

void print_time_reg();

//
//
//
LogFunc LOG_Func = nullptr;
void on_start(LogFunc logfunc) {
  LOG_Func = logfunc;

  if (LOG_Func) {
    LOG_Func("libinno : version 1.2");
  }

  //
  print_config();
  print_time_reg();

  //
  on_ntp_packet(0, 1 << 31);
}

//
//
//
void log(const char *str) {
  if (LOG_Func) {
    LOG_Func(str);
  }
}

void print_config() {
  char buf[1024 + 1]{0};
  snprintf(buf, 1024, "libinno : config from: %s", NTP_CONFIG.from.c_str());  log(buf);
  log("libinno : --------------------------");
  snprintf(buf, 1024, "libinno : \t debug_mode       (default: true)   : %s", NTP_CONFIG.debug_mode ? "true" : "false");    log(buf);
  snprintf(buf, 1024, "libinno : \t check_locked     (default: false)  : %s", NTP_CONFIG.check_locked ? "true" : "false");  log(buf);
  snprintf(buf, 1024, "libinno : \t check_offset     (default: false)  : %s", NTP_CONFIG.check_offset ? "true" : "false");  log(buf);
  snprintf(buf, 1024, "libinno : \t drift_ppm_thresh (default: 0)      : %d", NTP_CONFIG.drift_ppm_thresh);                 log(buf);
  snprintf(buf, 1024, "libinno : \t fpga_ts_thresh   (default: 1000,0) : %d", NTP_CONFIG.fpga_ts_thresh);                   log(buf);
  snprintf(buf, 1024, "libinno : \t retry_timeout    (default: 5)      : %d", NTP_CONFIG.retry_timeout);                    log(buf);
}

///////////////////////////////////////////////////////////////////////////////
// reg

// PS registers for PTP packet
#define PTP_TIMESTAMP_1 0x1C4
#define PTP_TIMESTAMP_2 0x1C8
#define PTP_TIMESTAMP_3 0x1CC
#define PTP_TIMESTAMP_4 0x1D0  // initiates the packet

#define TIMESTAMP_LOW_REG 0x1E4
#define TIMESTAMP_HIGH_REG 0x1E8

#define GPS_CONFIG 0x1B8

// GPS_CONFIG bit
#define GPS_CONFIG_PTP_EN_BIT 29
#define GPS_CONFIG_GPS_EN_BIT 28
#define GPS_CONFIG_NTP_EN_BIT 27

/* Definitions for peripheral AXILITE2REG_1 */
#define PL_BASEADDR 0x43C2000000L
#define PL_HIGHADDR 0x43C2000FFFL

/* Definitions for FPGA clock */
#define FPGA_MOTOR_CLK 125000000
#define FPGA_CLK_PERIOD_NS (1000000000 / FPGA_MOTOR_CLK)

//
// if read_result is NULL, then it is write operation;
// otherwise it is read operation.
//
int phyaddr_access(unsigned long target, uint32_t *read_result,
                   uint32_t writeval) {
  static unsigned mapped_size = 0;
  static int fd = -1;
  static void *map_base = NULL;
  static off_t page_offset_open = -1;

  char buf[1024 + 1]{0};

  uint16_t width =
      8 * sizeof(uint32_t);  // bytes to R/W from this register address

  unsigned page_size;

  mapped_size = page_size = getpagesize();
  off_t page_offset = target & ~(off_t)(page_size - 1);

  //
  unsigned offset_in_page = (unsigned)target & (page_size - 1);
  if (offset_in_page + width > page_size) {
    // This access spans pages, must map two pages
    if (mapped_size != page_size * 2) {
      // Page size request doesn't match previous page, disregard open page
      page_offset_open = -1;
    }
    mapped_size *= 2;
  }

  // open
  if (fd < 0 || page_offset != page_offset_open) {
    // New page must be mmaped, unmap existing page if exists
    if (map_base != NULL) {
      if (munmap(map_base, mapped_size) == -1) {
        snprintf(buf, 1024, "libinno : [error] munmap errno: %d", errno);
        log(buf);

        close(fd);
        fd = -1;
        return -1;
      }
      map_base = NULL;
    }

    // Truncate offset to a multiple of the page size, or mmap will fail.
    fd = open("/dev/mem", O_RDWR | O_SYNC);

    if (fd < 0) {
      snprintf(buf, 1024,
               "libinno : [error] open /dev/mem, fd = %d, errno = %d", fd,
               errno);
      log(buf);

      return -1;
    }

    snprintf(buf, 1024, "libinno : mmap length: %d, base: 0x%lX",
             mapped_size, page_offset);
    log(buf);

    map_base = mmap(NULL, mapped_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd,
                    page_offset);
    if (map_base == MAP_FAILED) {
      snprintf(buf, 1024, "libinno : [error] mmap errno : %d", errno);
      log(buf);

      close(fd);
      fd = -1;
      map_base = NULL;
      return -1;
    }

    page_offset_open = page_offset;
  }

  //
  volatile uint32_t *virt_addr = (uint32_t *)(map_base + offset_in_page);
  if (read_result) {
    *read_result = *(volatile uint32_t *)virt_addr;
    snprintf(buf, 1024,
             "libinno : virt_addr: 0x%lX = 0x%lX + 0x%X, read_value = 0x%X",
             virt_addr, map_base, offset_in_page,
             *(volatile uint32_t *)virt_addr);
    log(buf);

  } else {
    *(volatile uint32_t *)virt_addr = writeval;
    snprintf(buf, 1024,
             "libinno : virt_addr: 0x%lX = 0x%lX + 0x%X, write_value = 0x%X",
             virt_addr, map_base, offset_in_page, writeval);
    log(buf);

    // NOTE: The register must be readable and writable.
    uint32_t readback = *(volatile uint32_t *)virt_addr;
    if (readback != writeval) {
      snprintf(buf, 1024,
               "libinno : [error] readback from 0x%X 0x%X != write 0x%X.",
               target, readback, writeval);
      log(buf);
    }
  }

  return 0;
}

//
//
//
int reg_rd(uint16_t offset) {
  unsigned long addr = PL_BASEADDR + offset;

  uint32_t data = 0;
  phyaddr_access(addr, &data, 0);
  return data;
}

//
//
//
void reg_wr(uint16_t offset, uint32_t data) {
  unsigned long addr = PL_BASEADDR + offset;
  phyaddr_access(addr, NULL, data);
}

///////////////////////////////////////////////////////////////////////////////
//

//
//
//
static uint64_t get_fpga_timestamp() {
  uint32_t low_bits = (uint32_t)reg_rd(TIMESTAMP_LOW_REG);
  uint32_t high_bits = (uint32_t)reg_rd(TIMESTAMP_HIGH_REG);

  // LSB 4 bits of low_bits is not used
  uint64_t timestamp = ((uint64_t)high_bits) << 28 | (low_bits >> 4);

  return timestamp;
}

//
//
//
void reg_wr_resend_ntp_unlocked() {
  // resend the same packet as last valid one, set as "NOT LOCKED"
  uint32_t ptp_packet_word = reg_rd(PTP_TIMESTAMP_4);
  CLEAR_BIT(ptp_packet_word, 23);
  reg_wr(PTP_TIMESTAMP_4, ptp_packet_word);
}

//
//
//
void print_time_reg() {
  uint32_t val = reg_rd(GPS_CONFIG);

  if ((val >> GPS_CONFIG_NTP_EN_BIT) & 0x1) {
    log("libinno : sync mode : NTP");
  }

  if ((val >> GPS_CONFIG_PTP_EN_BIT) & 0x1) {
    log("libinno : sync mode : PTP");
  }

  if ((val >> GPS_CONFIG_GPS_EN_BIT) & 0x1) {
    log("libinno : sync mode : GPS");
  }
}

//
//
//
void on_ntp_packet(int64_t offset_ns, uint32_t flags) {
  static double s_local_time_pre = 0;
  static double s_local_fpga_diff_pre = 0;
  static uint8_t s_is_locked_once = 0;

  //
  char buf[1024]{0};
  snprintf(buf, 1024, "libinno : offset : %ld, flags : %u", offset_ns, flags);
  log(buf);

  if (!flags) {
    // reg_wr_resend_ntp_unlocked();
    return;
  }

  //
  snprintf(buf, 1024,
           "libinno : s_local_fpga_diff_pre:%f, s_local_time_pre : %f, "
           "s_is_locked_once : %d",
           s_local_fpga_diff_pre, s_local_time_pre, s_is_locked_once);
  log(buf);

  //
  int64_t ts_usec;
  double local_time;
  double local_fpga_diff, ppm_drift;
  uint64_t fpga_ts_avg, fpga_ts_diff;
  struct tm arm_ts;

  struct timeval tv;

  //
  uint64_t fpga_ts1, fpga_ts2;
  for (int idx = 0; idx < NTP_CONFIG.retry_timeout; idx++) {
    fpga_ts1 = get_fpga_timestamp();
    gettimeofday(&tv, NULL);
    fpga_ts2 = get_fpga_timestamp();

    arm_ts = *localtime(&tv.tv_sec);
    ts_usec = tv.tv_usec;

    if (NTP_CONFIG.debug_mode) {
      snprintf(buf, 1024,
               "libinno : fpga_ts1: 0x%lX; fpga_ts2: 0x%lX; tv: <%ld.%06ld>",
               fpga_ts1, fpga_ts2, (long int)(tv.tv_sec),
               (long int)(tv.tv_usec));
      log(buf);
    }

    // Adjusted FPGA local timestamp (# of cycles @ FPGA_MOTOR_CLK Hz)
    // Average of two reads offsetted by the system sub-second clock
    fpga_ts_avg = (fpga_ts1 + fpga_ts2) / 2;
    fpga_ts_diff = fpga_ts2 - fpga_ts1;

    local_time = (double)tv.tv_sec + ((double)tv.tv_usec) / (1000 * 1000);
    local_fpga_diff = local_time - (fpga_ts_avg / 125000000.0);

    double local_time_intervel = local_time - s_local_time_pre;
    if (local_time_intervel > 0) {
      ppm_drift = (local_fpga_diff - s_local_fpga_diff_pre) /
                  local_time_intervel * 1000000;

      snprintf(buf, 1024,
               "libinno : (%f - %f) / %f * 1000000 = %f, fpga_ts_avg - %ld, "
               "fpga_ts_diff - %ld",
               local_fpga_diff, s_local_fpga_diff_pre, local_time_intervel,
               ppm_drift, fpga_ts_avg, fpga_ts_diff);
      log(buf);
    } else {
      log("libinno : ERR - divide by zero attempted");
      continue;
    }

    if (fpga_ts_diff < NTP_CONFIG.fpga_ts_thresh) {
      break;
    }
  }

  //
  s_local_time_pre = local_time;
  s_local_fpga_diff_pre = local_fpga_diff;

  //
  if (NTP_CONFIG.check_locked) {
    s_is_locked_once = ((ppm_drift > -NTP_CONFIG.drift_ppm_thresh) &&
                        (ppm_drift < NTP_CONFIG.drift_ppm_thresh))
                           ? 1
                           : 0;
  } else {
    s_is_locked_once = 1;
  }

  if (NTP_CONFIG.debug_mode) {
    snprintf(buf, 1024,
             "libinno : s_is_locked_once - %d, fpga_ts_avg - %ld, local_time "
             "- %f, ppm_drift - %f, fpga_ts_diff - %ld",
             s_is_locked_once, fpga_ts_avg, local_time, ppm_drift,
             fpga_ts_diff);
    log(buf);
  }

  if (!s_is_locked_once) {
    reg_wr_resend_ntp_unlocked();
    return;
  }

  //
  uint64_t fpga_ts_adjust = fpga_ts_avg - (ts_usec * 1000 / FPGA_CLK_PERIOD_NS);

  uint8_t s0 = (uint8_t)(arm_ts.tm_sec % 10);
  uint8_t s1 = (uint8_t)(arm_ts.tm_sec / 10);
  uint8_t m0 = (uint8_t)(arm_ts.tm_min % 10);
  uint8_t m1 = (uint8_t)(arm_ts.tm_min / 10);
  uint8_t h0 = (uint8_t)(arm_ts.tm_hour % 10);
  uint8_t h1 = (uint8_t)(arm_ts.tm_hour / 10);
  uint8_t y0 = (uint8_t)(arm_ts.tm_year % 10);  // tm_year starts from 1900
  uint8_t y1 = (uint8_t)((arm_ts.tm_year % 100) / 10);
  uint8_t M0 = (uint8_t)((arm_ts.tm_mon + 1) % 10);  // tm_mon starts from 0
  uint8_t M1 = (uint8_t)((arm_ts.tm_mon + 1) / 10);
  uint8_t d0 = (uint8_t)(arm_ts.tm_mday % 10);
  uint8_t d1 = (uint8_t)(arm_ts.tm_mday / 10);

  // Overflow protection for 10bits
  int32_t offset_us = 0;
  uint8_t offset_10_100us = 0;

  constexpr int NS_10US = 1000 * 10;
  constexpr int NS_100US = 1000 * 10;
  constexpr int MAX_10US = 1023 * NS_10US;

  if (NTP_CONFIG.check_offset) {
    int64_t offset_abs = std::abs(offset_ns);

    offset_10_100us = offset_abs > MAX_10US ? 1 : 0;
    offset_abs /= (offset_10_100us ? NS_100US : NS_10US);
    offset_us = offset_abs > 0x3FF ? 0x3FF : offset_abs;
  }

  //
  uint32_t ptp_reg_1 = fpga_ts_adjust & 0xFFFFFFFF;
  uint32_t ptp_reg_2 = (s0 << 28) | ((fpga_ts_adjust >> 32) & 0x0FFFFFFF);
  uint32_t ptp_reg_3 = (M0 << 28) | (y1 << 24) | (y0 << 20) | (h1 << 16) |
                       (h0 << 12) | (m1 << 8) | (m0 << 4) | s1;
  uint32_t ptp_reg_4 = (1 << 31) | (1 << 23) | (offset_10_100us << 22) |
                       ((offset_us << 12) & 0x3FFFFF) | (d1 << 8) | (d0 << 4) |
                       M1;

  reg_wr(PTP_TIMESTAMP_1, ptp_reg_1);
  reg_wr(PTP_TIMESTAMP_2, ptp_reg_2);
  reg_wr(PTP_TIMESTAMP_3, ptp_reg_3);
  reg_wr(PTP_TIMESTAMP_4, ptp_reg_4);

  if (NTP_CONFIG.debug_mode) {
    snprintf(buf, 1024, "libinno , set reg: 0x%X, 0x%X, 0x%X, 0x%X", ptp_reg_1,
             ptp_reg_2, ptp_reg_3, ptp_reg_4);
    log(buf);

    snprintf(buf, 1024, "libinno , get reg: 0x%X, 0x%X, 0x%X, 0x%X",
             reg_rd(PTP_TIMESTAMP_1), reg_rd(PTP_TIMESTAMP_2),
             reg_rd(PTP_TIMESTAMP_3), reg_rd(PTP_TIMESTAMP_4));
    log(buf);
  }
}
