///////////////////////////////////////////////////////////////////////////////
//  _____                                  _
// |_   _|                                (_)
//   | |  _ __  _ __   _____   ___   _ ___ _  ___  _ __
//   | | | '_ \| '_ \ / _ \ \ / / | | / __| |/ _ \| '_  |
//  _| |_| | | | | | | (_) \ V /| |_| \__ \ | (_) | | | |
// |_____|_| |_|_| |_|\___/ \_/  \__,_|___/_|\___/|_| |_|
//
//
// Copyright (c) 2018  Innovusion, Inc.
// All Rights Reserved.
//
//		reg_functions.h
//
///////////////////////////////////////////////////////////////////////////////

#ifndef SRC_REG_FUNCTION_H_
#define SRC_REG_FUNCTION_H_

#include <stdio.h>
#include <string>

class NtpConfig {
 public:
  std::string from{"default"};

  bool debug_mode{true};          // print log
  bool check_locked{false};       //
  bool check_offset{false};       //
  int drift_ppm_thresh{0};        // 0: ignore
  int fpga_ts_thresh{1000 * 10};  //
  int retry_timeout{5};

 public:
  NtpConfig();
  ~NtpConfig();

  void Print();

 private:
  bool OpenFile();
};

#endif /* SRC_REG_FUNCTION_H_ */
