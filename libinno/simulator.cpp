
#include "iv_packet.h"

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

void logFunc(const char *str) { printf("%s\n", str); }

/* For long options that have no equivalent short option, use a
   non-character as a pseudo short option, starting with CHAR_MAX + 1.  */
enum { DEBUG_DATE_PARSING = CHAR_MAX + 1 };

static char const short_options[] = "d:f:i::r:Rs:u";

static struct option const long_options[] = {
    {"date", required_argument, NULL, 'd'},
    {"debug", no_argument, NULL, DEBUG_DATE_PARSING},
    {"file", required_argument, NULL, 'f'},
    {"interval", optional_argument, NULL, 'i'},
    {"reference", required_argument, NULL, 'r'},
    {"set", required_argument, NULL, 's'},
    {"uct", no_argument, NULL, 'u'},
    {"utc", no_argument, NULL, 'u'},
    {"universal", no_argument, NULL, 'u'},
    {NULL, 0, NULL, 0}};

int main(int argc, char *argv[]) {
  int interval_s = 2;

  int optc = 0;
  while ((optc = getopt_long(argc, argv, short_options, long_options, NULL)) !=
         -1) {
    char const *new_format = NULL;

    switch (optc) {
      case 'd':
        break;
      case DEBUG_DATE_PARSING:
        break;
      case 'f':
        break;
      case 'i':
        interval_s = ::atoi(optarg);
        if(interval_s < 1) {
          interval_s = 1;
        }
        break;
    }
  }

  //
  on_start(logFunc);

  //
  printf("interval_s: %d\n", interval_s);

  //
  int index = 0;
  while (true) {
    printf("\n\n--- %d\n", ++index);
    on_ntp_packet(1, 1);

    ::sleep(interval_s);
  }

  return 0;
}