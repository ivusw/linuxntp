///////////////////////////////////////////////////////////////////////////////
//  _____                                  _
// |_   _|                                (_)
//   | |  _ __  _ __   _____   ___   _ ___ _  ___  _ __
//   | | | '_ \| '_ \ / _ \ \ / / | | / __| |/ _ \| '_  |
//  _| |_| | | | | | | (_) \ V /| |_| \__ \ | (_) | | | |
// |_____|_| |_|_| |_|\___/ \_/  \__,_|___/_|\___/|_| |_|
//
//
// Copyright (c) 2018  Innovusion, Inc.
// All Rights Reserved.
//
//		config.c
//
///////////////////////////////////////////////////////////////////////////////

#include "config.h"

#include <fstream>
#include <iostream>
#include <regex>
#include <string>

NtpConfig::NtpConfig() { OpenFile(); }

NtpConfig::~NtpConfig() {}

//
//
//
void NtpConfig::Print() {
  printf("config from: %s\n", this->from.c_str());
  printf("--------------------------\n");

  printf("\t debug_mode       (default: true)   : %s\n", this->debug_mode ? "true" : "false");
  printf("\t check_locked     (default: false)  : %s\n", this->check_locked ? "true" : "false");
  printf("\t check_offset     (default: false)  : %s\n", this->check_offset ? "true" : "false");

  printf("\t drift_ppm_thresh (default: 0)      : %d\n", this->drift_ppm_thresh);
  printf("\t fpga_ts_thresh   (default: 1000,0) : %d\n", this->fpga_ts_thresh);
  printf("\t retry_timeout    (default: 5)      : %d\n", this->retry_timeout);
}

//
//
//
bool NtpConfig::OpenFile() {
  std::string config_file = "/mnt/config_firmware/ntp.conf";

  std::ifstream infile;
  infile.open(config_file, std::ios::in);
  if (!infile.is_open()) {
    return false;
  }

  //
  std::vector<std::string> lines;

  std::string buf;
  while (getline(infile, buf)) {
    lines.push_back(buf);
  }

  // std::regex re{"="};
  // std::vector<std::string> params = {
  //     std::sregex_token_iterator(line.begin(), line.end(), re, -1),
  //     std::sregex_token_iterator()};

  // if (params.size() != 2) {
  //   printf("config parse faild : %s\n", line.c_str());
  //   continue;
  // }

  // key = value
  std::regex re{R"(\W*(\w+)\W*=\D*(\d+)\D*)"};
  for (auto& line : lines) {
    std::smatch sm;
    if (regex_search(line, sm, re) == false || sm.size() != 3) {
      printf("config parse faild : %s\n", line.c_str());
      continue;
    }

    std::string key = sm[1].str();
    std::string val = sm[2].str();

    std::transform(key.begin(), key.end(), key.begin(), ::tolower);

    if (key == "check_offset") {
      this->check_offset = ::atoi(val.c_str()) > 0;
    } else if (key == "check_locked") {
      this->check_locked = ::atoi(val.c_str()) > 0;
    } else if (key == "debug") {
      this->debug_mode = ::atoi(val.c_str()) > 0;
    } else if (key == "drift_ppm_thresh") {
      this->drift_ppm_thresh = ::atoi(val.c_str());
    } else if (key == "fpga_ts_thresh") {
      this->fpga_ts_thresh = ::atoi(val.c_str());
    } else if (key == "retry_timeout") {
      this->retry_timeout = ::atoi(val.c_str());
    } else {
      printf("unknown key: %s", key.c_str());
    }

    printf("config : %s\n", line.c_str());
  }

  this->from = config_file;
}